<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pabhoz
 */
interface IAlienFactory {
    public function getAlien($nombre, $edad, $especie, $planeta);
    public function getJupiterAlien($nombre, $edad, $especie);
    public function getMarsAlien($nombre, $edad, $especie);
    public function getMoonAlien($nombre, $edad, $especie);
    public function getPlutoAlien($nombre, $edad, $especie);
    public function getSaturnAlien($nombre, $edad, $especie);
    public function getVenusAlien($nombre, $edad, $especie);
}
