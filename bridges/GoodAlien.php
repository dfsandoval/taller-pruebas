<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GoodAlien
 *
 * @author pabhoz
 */
abstract class GoodAlien extends Alien implements IGoodAlien{
    
    protected $moral = "bueno";
    protected $planeta="Marte, Luna y Venus";
    
   public function interact() {
        return self::COMUNICACION . " dice: Hola terricola mi nombre es ".
                $this->getNombre().", vinimos en son de paz";
    }
    
    public function salvarPlaneta(Planet $planeta){
        $planeta->setEstado("a salvo");
    }
    public function llamarACasa(){
        return "Llamando a Caaaaasaaaaa";
    }
}
