<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BadAlien
 *
 * @author pabhoz
 */
abstract class BadAlien extends Alien implements IBadAlien{
    
    protected $moral = "malo";
    protected $planeta="Plutón, Jupiter y Saturno";
     public function interact(){
        return self::COMUNICACION ." dice: Hola terricola, rindanse ante la invasión de "
                . $this->getNombre();
    }
    
    public function destruirPlaneta(Planet $planeta){
        $planeta->setEstado("destruido");
    }
    public function pedirRefuerzos(){
        return "Solicitando refuerzos Muahahahaha";
    }
}
